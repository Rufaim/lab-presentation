<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<title>Presentation WGAN</title>

		<link rel="stylesheet" href="css/reset.css">
		<link rel="stylesheet" href="css/reveal.css">
		<link rel="stylesheet" href="css/theme/black.css">

		<!-- Internal CSS styles -->
		<link rel="stylesheet" href="presentation.css">

		<!-- Theme used for syntax highlighted code -->
		<link rel="stylesheet" href="plugin/highlight/monokai.css">

		<!-- Custom style edits -->
		<link rel="stylesheet" href="css/reveal-style-edits.css">

	</head>
	<body>
		<div class="reveal">
			<div class="slides">
				<!-- HEAD -->
				<div class="permanent">
					<div class="permanent-logo">
						<img src="assets/mcgill-shield.png" alt="">
					</div>
					<div class="permanent-address">
						<a href="https://rufaim.gitlab.io/lab-presentation/">rufaim.gitlab.io/lab-presentation</a>
					</div>
				</div>
				
				
				<!-- SLIDES -->
				<section data-hide-slide-number="true" data-visibility="uncounted"> 
					<!--Title slide-->
					<h1>Wasserstein GAN</h1>
					
					<h3>Pavel Rumiantsev</h3>
				</section>


				<section>
					<h4>Problem formulation</h4>
					<p>For a set of real world datapoint
						<span class="highlight">$\{x_i\}$</span>$_{i=[1..n]}$
						with data distribution <span class="highlight">$\P_r$</span> we need to maximize log-likelihood
						of a parametric distribution <span class="highlight">$\P_\theta$</span>
						\[\max_\theta \frac{1}{n} \sum_i^n P_\theta (x_i)\]
						or minimize $KL(\P_r||\P_\theta)$ 
					</p>
				</section>

				<section>
					<!-- GAN problem -->
					Simultaneously optimizing generator <span class="highlight">$G$</span> 
					and discriminator <span class="highlight">$D$</span> in a minimax game manner
					\[\min_G \max_D \; \E_{x\sim\P_r} \log D(x) + \E_{z\sim\P_\theta}(1-\log D(G(z)))\]

					<div class="reference r-frame">
						<a href="https://arxiv.org/abs/1406.2661" target="_blank">
							<img data-src="assets/arxiv-logomark.svg" alt="arXiv.org">
							<p>Goodfellow, Ian, et al. 
								<span class="name">&ldquo;Generative adversarial nets&rdquo;</span>
								<span class="year">(2014)</span>
							</p>
						</a> 
					</div>
				</section>

				<section>
					<pre class="bigcode"><code data-line-numbers data-trim data-noescape class="language-python hljs">
						import tensorflow as tf

						generator_model = tf.keras.Model(...)
						discriminator_model = tf.keras.Model(...)
						cross_entropy_loss = tf.keras.losses.BinaryCrossentropy(from_logits=True)

						def generator_loss(noise, training=None):
							generated_images = generator_model(noise, training=training)
							fake_output = discriminator_model(generated_images, training=training)
							return cross_entropy_loss(tf.ones_like(fake_output), fake_output)		
						
						def discriminator_gan_loss(noise, images, training=None):
							generated_images = generator_model(noise, training=training)

							real_output = discriminator_model(images, training=training)
							fake_output = discriminator_model(generated_images, training=training)

							real_loss = cross_entropy_loss(tf.ones_like(real_output), real_output)
							fake_loss = cross_entropy_loss(tf.zeros_like(fake_output), fake_output)
							return real_loss + fake_loss
						</code></pre>
				</section>

				<section>
					<h3>Known problems of <span class="highlight" style="text-transform: none;">GANs</span></h3>
					<ul>
						<li>Loss is unrelated to convergence</li>
						<li>Mode collapse</li>
						<li>Unstable</li>
						<li>Hyperparameter sensitivity</li>
						<li>Saturated gradients</li>
					</ul>
				</section>

				<section data-auto-animate data-auto-animate-unmatched="fade">
					<h3>Wasserstein GAN</h3>
					<p>
						\[\min_G \max_{\lVert D \rVert_L < 1} \; \E_{x\sim\P_r} D(x) - \E_{z\sim\P_\theta} D(G(z))\]
					</p>
					<div>
						<span class="highlight">$\lVert D \rVert_L < 1$</span> is hard to work with.

						<div class="r-frame reference">
							<a href="https://arxiv.org/abs/1406.2661" target="_blank">
								<img data-src="assets/arxiv-logomark.svg" alt="arXiv.org">
								<p>Arjovsky, Martin, Soumith Chintala, and Léon Bottou
									<span class="name">&ldquo;Wasserstein generative adversarial networks&rdquo;</span>
									<span class="year">(2017)</span>
								</p>
							</a>
						</div>
					</div>
				</section>	
				<section data-auto-animate data-auto-animate-unmatched="fade">
					<h3>Wasserstein GAN</h3>
					<div class="r-stack">
						<img class="fragment fade-out" width="60%" data-fragment-index="1" src="assets/wgan_gradients.png" alt="wgan-gradients">
						<img class="fragment fade-in" width="60%"  data-fragment-index="1" src="assets/wgan-discriminator-cost.png" alt="wgan-discriminator-cost">
					</div>
					<div class="r-stack">
						<p class="fragment fade-out" data-fragment-index="1">Unsaturated gradients</p>
						<p class="fragment fade-in" data-fragment-index="1">Loss is correlated with quality &#128077;</p>
					</div>
				</section>

				<section>
					<h3>Known problems of 
						<div class="r-stack">
							<span class="fragment fade-out highlight" style="text-transform: none;" data-fragment-index="1">GANs</span>
							<span class="fragment fade-in highlight" style="text-transform: none;" data-fragment-index="1">WGANs</span>
						</div>
						</h3>
					<ul>
						<li class="fragment strike semi-fade-out" data-fragment-index="1">Loss is unrelated to convergence</li>
						<li>Mode collapse</li>
						<li>Unstable</li>
						<li>Hyperparameter sensitivity</li>
						<li class="fragment strike semi-fade-out" data-fragment-index="1">Saturated gradients</li>
						<li class="fragment fade-in" data-fragment-index="1">1-Lipchitz discriminator</li>
					</ul>
				</section>
					
				<section>
					<div class="r-stack">
						<img width="85%" src="assets/wgan-algorithm.png" alt="wgan-algorithm">
						<div class="fragment fade-in"  data-fragment-index="1" style="border: 6px solid red; border-radius: 8px; width: 18rem; height: 1.5rem; transform: translateY(5rem) translateX(-16rem);"></div>
					</div>
					<pre class="fragment fade-in" data-fragment-index="1"><code class="language-python hljs">weights = [w.assign(tf.clip_by_value(w, -c, c)) for w in weights]</code></pre>
				</section>

				<section>
					<h3>Improved training of WGAN</h3>
					<p style="margin-left: -10%;">
						\[\min_G \max_{\lVert D \rVert_L < 1} \; \E_{x\sim\P_r} \E_{z\sim\P_\theta} D(x) - D(G(z)) + \lambda ( \lVert \nabla D(\hat{x}) \rVert_2 - 1)^2\]
						<div style="text-align: left; margin-top: -3%;">where $\hat{x} = \epsilon x + (1- \epsilon) z$</div>
					</p>
					<div>
						$\lVert D \rVert_L < 1$ is enforced as <span class="highlight">regularization</span>.

						<div class="r-frame reference">
							<a href="https://arxiv.org/abs/1406.2661" target="_blank">
								<img data-src="assets/arxiv-logomark.svg" alt="arXiv.org">
								<p>Gulrajani, Ishaan, et al.
									<span class="name">&ldquo;Improved training of wasserstein gans&rdquo;</span>
									<span class="year">(2017)</span>
								</p>
							</a>
						</div>
					</div>
				</section>

				<section>
					<pre class="bigcode"><code data-line-numbers data-trim data-noescape class="language-python hljs">
						import tensorflow as tf

						generator_model = tf.keras.Model(...)
						discriminator_model = tf.keras.Model(...)

						def generator_loss(noise, training=None):
							generated_images = generator_model(noise, training=training)
							fake_output = discriminator_model(generated_images, training=training)
							return -tf.math.reduce_mean(fake_output)		
						
						</code></pre>
				</section>

				<section>
					<pre class="bigcode"><code data-line-numbers data-trim data-noescape data-ln-start-from="11" class="language-python hljs">
						def discriminator_gan_loss(noise, images, lmbd, epsilon, training=None):
							generated_images = generator_model(noise, training=training)

							real_output = discriminator_model(images, training=training)
							fake_output = discriminator_model(generated_images, training=training)
					
							mixed_images = generated_images + epsilon * (images - generated_images)
							with tf.GradientTape() as tape:
								tape.watch(mixed_images)
								mixed_scores = discriminator_model(mixed_images, training=training)
					
							grad = tape.gradient(mixed_scores, mixed_images)
							grad_norm = tf.norm(grad, axis=[1, 2, 3])
							l1_penalty = tf.reduce_mean((grad_norm - 1) ** 2)
							return tf.reduce_mean(fake_output) - tf.reduce_mean(real_output) + lmbd * l2_penalty
						</code></pre>
				</section>

				<section data-auto-animate>
					<div class="r-hstack" style="margin-left: -15%; width: 130%; padding-top: 12px;">
						<div>
							<img src="assets/results/gan-results1.png" alt="gan-results1">
							<p>GAN</p>
						</div>
						<div>
							<img src="assets/results/wgan-clip-results1.png" alt="wgan-clip-results1">
							<p>WGAN clip</p>
						</div>
						<div>
							<img src="assets/results/wgan-gp-results1.png" alt="wgan-gp-results1">
							<p>WGAN penalty</p>
						</div>
					</div>
					<p>Generator and discriminator are <span data-id="1" class="highlight">small CNNs.</span></p>
				</section>

				<section data-auto-animate>
					<div class="r-hstack" style="margin-left: -15%; width: 130%; padding-top: 12px;">
						<div>
							<img src="assets/results/gan-results2.png" alt="gan-results1">
							<p>GAN</p>
						</div>
						<div>
							<img src="assets/results/wgan-clip-results2.png" alt="wgan-clip-results1">
							<p>WGAN clip</p>
						</div>
						<div>
							<img src="assets/results/wgan-gp-results2.png" alt="wgan-gp-results1">
							<p>WGAN penalty</p>
						</div>
					</div>
					<p>Generator and discriminator are <span data-id="1" class="highlight">ResNet-101.</span></p>
				</section>

				<section>
					<!-- Stability analisys -->
					<h3>Dirac-GAN counterexample</h3>
					<div>
						<div>Generator $G_\theta(x) = \theta$</div>
						<div>Discriminator $D_\phi(x) = \phi x$</div>

						<div class="r-frame reference">
							<a href="https://arxiv.org/abs/1406.2661" target="_blank">
								<img data-src="assets/arxiv-logomark.svg" alt="arXiv.org">
								<p>Mescheder, Lars, Andreas Geiger, and Sebastian Nowozin
									<span class="name">&ldquo;Which training methods for GANs do actually converge?&rdquo;</span>
									<span class="year">(2018)</span>
								</p>
							</a>
						</div>

						<p>
							<br>$Loss(\theta,\phi) = f(D_\phi(G_\theta(1))) + f(0)$ <br><br>
							where for <span class="highlight">GAN</span>$f(x) = -\log(1+\exp(-x))$ <br>
							and for <span class="highlight"> WGAN </span> $f(x) = x$
						</p>
					</div>
				</section>
					
				<section>
					<h3>Stability of <span class="highlight" style="text-transform: none;">GANs</span> on the counterexample</h3>
					<div class="r-hstack" style="margin-left: -5%; width: 110%;">
						<div>
							<img src="assets/stability/gan-stability.png" alt="gan-stability">
							<p>GAN</p>
						</div>
						<div>
							<img src="assets/stability/wgan-clip-stability.png" alt="wgan-clip-stability">
							<p>WGAN clip</p>
						</div>
						<div>
							<img src="assets/stability/wgan-gp-stability.png" alt="wgan-gp-stability">
							<p>WGAN penalty</p>
						</div>
					</div>
				</section>

				<section>
						<div class="r-hstack">
							<div>
								<img style="margin-bottom: 0;" src="assets/gan-gifs/gan.gif" alt="gan-gif">
								<p style="margin-top: 0;">GAN on MNIST</p>
							</div>
							<div>
								<img style="margin-bottom: 0;" src="assets/gan-gifs/wgan.gif" alt="wgan-gif">
								<p style="margin-top: 0;">WGAN on MNIST</p>
							</div>
						</div>
						<div class="r-hstack">
							<a href="https://github.com/Rufaim/Wasserstein-GAN">
								<img src="assets/GitHub-Mark-Light-64px.png" alt="github-logo">
							</a>
							<a style="margin-left: 8px;" href="https://github.com/Rufaim/Wasserstein-GAN">Rufaim/Wasserstein-GAN</a>
						</div>
					
				</section>
			</div>
		</div>

		<script src="css/reveal.js"></script>
		<script src="plugin/zoom/zoom.js"></script>
		<script src="plugin/notes/notes.js"></script>
		<script src="plugin/search/search.js"></script>
		<script src="plugin/markdown/markdown.js"></script>
		<script src="plugin/math/math.js"></script>
		<script src="plugin/highlight/highlight.js"></script>

		<script>
			function toggleSlideNumberVisibility(isOn) {
				let slide_field = document.getElementsByClassName("slide-number")[0];
				if (isOn) {
					slide_field.style.visibility = "visible";
				} else {
					slide_field.style.visibility = "hidden";
				}
				
			}

			Reveal.on( 'ready', event => {
				const isSnOn = (event.currentSlide.dataset.hideSlideNumber !== 'true');
				toggleSlideNumberVisibility(isSnOn);
			} );

			Reveal.on( 'slidechanged', event => {
				const isSnOn = (event.currentSlide.dataset.hideSlideNumber !== 'true');
				toggleSlideNumberVisibility(isSnOn);
			} );

			// More info about initialization & config:
			// - https://revealjs.com/initialization/
			// - https://revealjs.com/config/
			Reveal.initialize({

				// Display presentation control arrows
				controls: false,

				// Help the user learn the controls by providing hints, for example by
				// bouncing the down arrow when they first encounter a vertical slide
				controlsTutorial: false,

				// Determines where controls appear, "edges" or "bottom-right"
				controlsLayout: 'bottom-right',

				// Visibility rule for backwards navigation arrows; "faded", "hidden"
				// or "visible"
				controlsBackArrows: 'faded',

				// Display a presentation progress bar
				progress: true,

				// Display the page number of the current slide
				// - true:    Show slide number
				// - false:   Hide slide number
				//
				// Can optionally be set as a string that specifies the number formatting:
				// - "h.v":   Horizontal . vertical slide number (default)
				// - "h/v":   Horizontal / vertical slide number
				// - "c":   Flattened slide number
				// - "c/t":   Flattened slide number / total slides
				//
				// Alternatively, you can provide a function that returns the slide
				// number for the current slide. The function should take in a slide
				// object and return an array with one string [slideNumber] or
				// three strings [n1,delimiter,n2]. See #formatSlideNumber().
				slideNumber: "c/t",

				// Can be used to limit the contexts in which the slide number appears
				// - "all":      Always show the slide number
				// - "print":    Only when printing to PDF
				// - "speaker":  Only in the speaker view
				showSlideNumber: 'all',

				// Use 1 based indexing for # links to match slide number (default is zero
				// based)
				hashOneBasedIndex: false,

				// Add the current slide number to the URL hash so that reloading the
				// page/copying the URL will return you to the same slide
				hash: true,

				// Flags if we should monitor the hash and change slides accordingly
				respondToHashChanges: true,

				// Push each slide change to the browser history.  Implies `hash: true`
				history: false,

				// Enable keyboard shortcuts for navigation
				keyboard: true,

				// Optional function that blocks keyboard events when retuning false
				//
				// If you set this to 'focused', we will only capture keyboard events
				// for embedded decks when they are in focus
				keyboardCondition: null,

				// Disables the default reveal.js slide layout (scaling and centering)
				// so that you can use custom CSS layout
				disableLayout: false,

				// Enable the slide overview mode
				overview: true,

				// Vertical centering of slides
				center: true,

				// Enables touch navigation on devices with touch input
				touch: true,

				// Loop the presentation
				loop: false,

				// Change the presentation direction to be RTL
				rtl: false,

				// Changes the behavior of our navigation directions.
				//
				// "default"
				// Left/right arrow keys step between horizontal slides, up/down
				// arrow keys step between vertical slides. Space key steps through
				// all slides (both horizontal and vertical).
				//
				// "linear"
				// Removes the up/down arrows. Left/right arrows step through all
				// slides (both horizontal and vertical).
				//
				// "grid"
				// When this is enabled, stepping left/right from a vertical stack
				// to an adjacent vertical stack will land you at the same vertical
				// index.
				//
				// Consider a deck with six slides ordered in two vertical stacks:
				// 1.1    2.1
				// 1.2    2.2
				// 1.3    2.3
				//
				// If you're on slide 1.3 and navigate right, you will normally move
				// from 1.3 -> 2.1. If "grid" is used, the same navigation takes you
				// from 1.3 -> 2.3.
				navigationMode: 'default',

				// Randomizes the order of slides each time the presentation loads
				shuffle: false,

				// Turns fragments on and off globally
				fragments: true,

				// Flags whether to include the current fragment in the URL,
				// so that reloading brings you to the same fragment position
				fragmentInURL: true,

				// Flags if the presentation is running in an embedded mode,
				// i.e. contained within a limited portion of the screen
				embedded: false,

				// Flags if we should show a help overlay when the question-mark
				// key is pressed
				help: true,

				// Flags if it should be possible to pause the presentation (blackout)
				pause: true,

				// Flags if speaker notes should be visible to all viewers
				showNotes: false,

				// Global override for autolaying embedded media (video/audio/iframe)
				// - null:   Media will only autoplay if data-autoplay is present
				// - true:   All media will autoplay, regardless of individual setting
				// - false:  No media will autoplay, regardless of individual setting
				autoPlayMedia: null,

				// Global override for preloading lazy-loaded iframes
				// - null:   Iframes with data-src AND data-preload will be loaded when within
				//           the viewDistance, iframes with only data-src will be loaded when visible
				// - true:   All iframes with data-src will be loaded when within the viewDistance
				// - false:  All iframes with data-src will be loaded only when visible
				preloadIframes: null,

				// Can be used to globally disable auto-animation
				autoAnimate: true,

				// Optionally provide a custom element matcher that will be
				// used to dictate which elements we can animate between.
				autoAnimateMatcher: null,

				// Default settings for our auto-animate transitions, can be
				// overridden per-slide or per-element via data arguments
				autoAnimateEasing: 'ease',
				autoAnimateDuration: 0.8,
				autoAnimateUnmatched: true,

				// CSS properties that can be auto-animated. Position & scale
				// is matched separately so there's no need to include styles
				// like top/right/bottom/left, width/height or margin.
				autoAnimateStyles: [
				'opacity',
				'color',
				'background-color',
				'padding',
				'font-size',
				'line-height',
				'letter-spacing',
				'border-width',
				'border-color',
				'border-radius',
				'outline',
				'outline-offset'
				],

				// Controls automatic progression to the next slide
				// - 0:      Auto-sliding only happens if the data-autoslide HTML attribute
				//           is present on the current slide or fragment
				// - 1+:     All slides will progress automatically at the given interval
				// - false:  No auto-sliding, even if data-autoslide is present
				autoSlide: 0,

				// Stop auto-sliding after user input
				autoSlideStoppable: true,

				// Use this method for navigation when auto-sliding (defaults to navigateNext)
				autoSlideMethod: null,

				// Specify the average time in seconds that you think you will spend
				// presenting each slide. This is used to show a pacing timer in the
				// speaker view
				defaultTiming: null,

				// Enable slide navigation via mouse wheel
				mouseWheel: true,

				// Opens links in an iframe preview overlay
				// Add `data-preview-link` and `data-preview-link="false"` to customise each link
				// individually
				previewLinks: false,

				// Exposes the reveal.js API through window.postMessage
				postMessage: true,

				// Dispatches all reveal.js events to the parent window through postMessage
				postMessageEvents: false,

				// Focuses body when page changes visibility to ensure keyboard shortcuts work
				focusBodyOnPageVisibilityChange: true,

				// Transition style
				transition: 'slide', // none/fade/slide/convex/concave/zoom

				// Transition speed
				transitionSpeed: 'default', // default/fast/slow

				// Transition style for full page slide backgrounds
				backgroundTransition: 'fade', // none/fade/slide/convex/concave/zoom

				// The maximum number of pages a single slide can expand onto when printing
				// to PDF, unlimited by default
				pdfMaxPagesPerSlide: Number.POSITIVE_INFINITY,

				// Prints each fragment on a separate slide
				pdfSeparateFragments: true,

				// Offset used to reduce the height of content within exported PDF pages.
				// This exists to account for environment differences based on how you
				// print to PDF. CLI printing options, like phantomjs and wkpdf, can end
				// on precisely the total height of the document whereas in-browser
				// printing has to end one pixel before.
				pdfPageHeightOffset: -1,

				// Number of slides away from the current that are visible
				viewDistance: 5,

				// Number of slides away from the current that are visible on mobile
				// devices. It is advisable to set this to a lower number than
				// viewDistance in order to save resources.
				mobileViewDistance: 2,

				// The display mode that will be used to show slides
				display: 'block',

				// Hide cursor if inactive
				hideInactiveCursor: true,

				// Time before the cursor is hidden (in ms)
				hideCursorTime: 500,

				mathjax3: {
					config: 'TeX-AMS_HTML-full',
					tex: {
						macros: {
							P: '\\mathbb{P}',
							E: '\\mathbb{E}',
						},
						inlineMath: [ [ '$', '$' ], [ '\\(', '\\)' ]  ]
					},
					options: {
						skipHtmlTags: [ 'script', 'noscript', 'style', 'textarea', 'pre' ]
					},
				},

				// Script dependencies to load
				dependencies: [],

				// Learn about plugins: https://revealjs.com/plugins/
				plugins: [ RevealZoom, RevealHighlight, RevealSearch, RevealNotes, RevealMath.MathJax3 ]
			});

			Reveal.configure({
				keyboard: {
					13: 'next', // go to the next slide when the ENTER key is pressed
					8: 'back', // go to the previous slide when the BACKSPACE key is pressed
				}
			});

			

		</script>
	</body>
</html>
